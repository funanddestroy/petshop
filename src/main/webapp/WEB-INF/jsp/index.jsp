<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="spr" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <title>Main Page</title>

    <style type="text/css">
        #story {
            width: 350px;
            background: #ccc;
            padding: 5px;
            padding-right: 20px;
            border: solid 1px black;
            float: left;
        }
        #run_away {
            width: 350px;
            background: #ccc;
            padding: 5px;
            padding-right: 20px;
            border: solid 1px black;
            float: left;
        }
        #pet_list {
            width: 450px;
            background: #fc0;
            padding: 5px;
            border: solid 1px black;
            float: left;
            top: 40px;
            left: -70px;
        }
    </style>

</head>

<body>

<%
    response.setIntHeader("Refresh", 1);
%>

<div id="buy_and_sell">
    <spring:form action="index" method="post">
        <input type="hidden" name="status" value="buy"/>
        <input type="submit" value="Buy"/>
    </spring:form>
    <spring:form action="index" method="post">
        <input type="hidden" name="status" value="sell"/>
        <input type="submit" value="Sell"/>
    </spring:form>
</div>

<div id="pet_list">
    <h3>Pet List</h3>
    <c:forEach var="animal" items="${animals}">
        ${animal}
        <br/>
    </c:forEach>
</div>

<div id="story">
    <h3>Story</h3>
    <c:forEach var="stor" items="${story}">
        ${stor}
        <br/>
    </c:forEach>
</div>

<div id="run_away">
    <h3>Run Away Animals</h3>
    <c:forEach var="run_away_animal" items="${run_away_animals}">
        ${run_away_animal}
        <br/>
    </c:forEach>
</div>

</body>

</html>