package ru.petshop.jersey;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.petshop.services.PetShopStorage;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Component
@Path("/animals")
public class RestService {

    @Autowired
    private PetShopStorage petShopStorage;

    @GET
    @Path("/{param}")
    public Response getMsg(@PathParam("param") String msg) {

        String output = "Jersey say : " + msg;

        return Response.status(200).entity(output).build();

    }

    @GET
    @Path("/list")
    public Response getListAnimals() {

        return Response.status(200).entity(getJsonAnimalList()).build();

    }

    private String getJsonAnimalList() {

        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.writeValueAsString(petShopStorage.getAnimals());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return "";
    }
}
