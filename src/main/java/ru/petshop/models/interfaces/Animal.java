package ru.petshop.models.interfaces;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import ru.petshop.models.Cat;
import ru.petshop.models.Dog;
import ru.petshop.models.Shark;
import ru.petshop.models.Wolf;

/**
 * An interface that implements methods for working with animals.
 *
 * @version 0.1
 * @autor Poltavets Dmitry
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Cat.class, name = "cat"),
        @JsonSubTypes.Type(value = Dog.class, name = "dog"),
        @JsonSubTypes.Type(value = Wolf.class, name = "wolf"),
        @JsonSubTypes.Type(value = Shark.class, name = "shark")})
public interface Animal {

    /**
     * method of obtaining the value of the breed {@link Animal}
     *
     * @return returns an animal breed
     */
    String getBreed();

    /**
     * method of obtaining the value of the name {@link Animal}
     *
     * @return returns the name of the animal
     */
    String getName();

    /**
     * method of obtaining value value {@link Animal}
     *
     * @return returns the cost of an animal
     */
    Integer getCost();

    /**
     * method of obtaining the value of the character {@link Animal}
     *
     * @return returns the character of the animal
     */
    String getCharacter();

    /**
     * method of obtaining the value of the degree of contamination {@link Animal}
     *
     * @return returns the degree of contamination of the animal
     */
    Integer getDegreeOfPollution();

    /**
     * method of assigning a new value of the degree of contamination {@link Animal}
     *
     * @param degreeOfPollution - new degree of animal contamination
     */
    void setDegreeOfPollution(Integer degreeOfPollution);

    /**
     * method of obtaining a value of the type {@link Animal}
     *
     * @return returns the type of animal
     */
    String getType();

}
