package ru.petshop.models.enums;

public enum WolfType {
    Species,
    Gray,
    Arctic,
    Red,
    Indian,
    Himalayan,
    Ethiopian,
    Eastern
}
