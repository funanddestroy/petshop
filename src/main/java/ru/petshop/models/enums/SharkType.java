package ru.petshop.models.enums;

public enum SharkType {
    GreatWhite,
    Tiger,
    Whale,
    Bull,
    Hammerhead,
    Goblin,
    Mako,
    Blue,
    Lemon,
    Basking,
    Megamouth,
    Prehistoric
}
