package ru.petshop.models.abstractclasses;

public abstract class Predator extends AbstractAnimal {

    public Predator() {
    }

    public Predator(String breed, String name, Integer cost, String character) {
        super(breed, name, cost, character);
    }
}
