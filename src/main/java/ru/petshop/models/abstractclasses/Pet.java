package ru.petshop.models.abstractclasses;

public abstract class Pet extends AbstractAnimal {

    public Pet() {
    }

    public Pet(String breed, String name, Integer cost, String character) {
        super(breed, name, cost, character);
    }
}
