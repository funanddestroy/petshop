package ru.petshop.models.abstractclasses;

import java.util.Objects;

public abstract class AbstractAnimal {

    protected String breed;
    protected String name;
    protected Integer cost;
    protected String character;
    protected String type;
    protected Integer degreeOfPollution;

    @Override
    public String toString() {
        return type + ": {" + breed + ", " + name + ", " + cost + ", " + character + ", " + degreeOfPollution + '}';
    }

    public AbstractAnimal() {
    }

    public AbstractAnimal(String breed, String name, Integer cost, String character) {
        this.breed = breed;
        this.name = name;
        this.cost = cost;
        this.character = character;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractAnimal that = (AbstractAnimal) o;
        return Objects.equals(breed, that.breed) &&
                Objects.equals(name, that.name) &&
                Objects.equals(cost, that.cost) &&
                Objects.equals(character, that.character);
    }

    @Override
    public int hashCode() {
        return Objects.hash(breed, name, cost, character);
    }
}
