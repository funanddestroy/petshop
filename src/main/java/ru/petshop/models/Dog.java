package ru.petshop.models;

import ru.petshop.models.abstractclasses.Pet;
import ru.petshop.models.enums.DogBreed;
import ru.petshop.models.enums.PetCharacter;
import ru.petshop.models.enums.PetName;
import ru.petshop.models.interfaces.Animal;
import ru.petshop.Constants;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Random;

@XmlRootElement(name = "dog")
public class Dog extends Pet implements Animal {

    public Dog() {
        Random random = new Random();

        super.breed = getRandomBreed(random);
        super.name = getRandomName(random);
        super.cost = getRandomCost(random);
        super.character = getRandomCharacter(random);
        super.type = "dog";
        super.degreeOfPollution = 0;
    }

    private String getRandomBreed(Random random) {
        int pick = random.nextInt(DogBreed.values().length);
        return DogBreed.values()[pick].toString();
    }

    private String getRandomName(Random random) {
        int pick = random.nextInt(PetName.values().length);
        return PetName.values()[pick].toString();
    }

    private Integer getRandomCost(Random random) {
        return random.nextInt(Constants.MAX_COAST);
    }

    private String getRandomCharacter(Random random) {
        int pick = random.nextInt(PetCharacter.values().length);
        return PetCharacter.values()[pick].toString();
    }

    public Dog(String breed, String name, Integer cost, String character) {
        super(breed, name, cost, character);
    }

    @XmlElement
    public String getBreed() {
        return breed;
    }

    @XmlElement
    public String getName() {
        return name;
    }

    @XmlElement
    public Integer getCost() {
        return cost;
    }

    @XmlElement
    public String getCharacter() {
        return character;
    }

    @XmlElement
    public Integer getDegreeOfPollution() {
        return degreeOfPollution;
    }

    public String getType() {
        return type;
    }

    public void setBreed(String breed) {
        super.breed = breed;
    }

    public void setName(String name) {
        super.name = name;
    }

    public void setCost(Integer cost) {
        super.cost = cost;
    }

    public void setCharacter(String character) {
        super.character = character;
    }

    public void setDegreeOfPollution(Integer degreeOfPollution) {
        super.degreeOfPollution = degreeOfPollution;
    }
}
