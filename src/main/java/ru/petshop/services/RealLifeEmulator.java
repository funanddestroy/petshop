package ru.petshop.services;

import java.util.Random;

public class RealLifeEmulator implements Runnable {

    private PetShopStorage petShopStorage;
    private BuyAndSell buyAndSell;

    public RealLifeEmulator() {
    }

    public void run() {
        Random random = new Random();
        while (true) {
            int pick = random.nextInt(10);
            switch (pick) {
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    buyAndSell.buy();
                    break;
                case 4:
                    buyAndSell.buy();
                    break;
                case 5:
                    break;
                case 6:
                    buyAndSell.sell();
                    break;
                case 7:
                    petShopStorage.runAway();
                    break;
            }

            sleep(1000);
        }
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void setPetShopStorage(PetShopStorage petShopStorage) {
        this.petShopStorage = petShopStorage;
    }

    public PetShopStorage getPetShopStorage() {
        return petShopStorage;
    }

    public void setBuyAndSell(BuyAndSell buyAndSell) {
        this.buyAndSell = buyAndSell;
    }

    public BuyAndSell getBuyAndSell() {
        return buyAndSell;
    }
}
