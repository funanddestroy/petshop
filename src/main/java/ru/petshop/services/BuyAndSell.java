package ru.petshop.services;

import ru.petshop.Constants;
import ru.petshop.models.Cat;
import ru.petshop.models.Dog;
import ru.petshop.models.Shark;
import ru.petshop.models.Wolf;
import ru.petshop.models.interfaces.Animal;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class BuyAndSell {

    private List<String> story;
    private PetShopStorage petShopStorage;
    private PrintingService printingService;

    public BuyAndSell() {
        story = new CopyOnWriteArrayList<>();
    }

    public void buy(Animal animal) {
        balanceListStore();

        String event = "Buying of " + animal.getClass().getSimpleName() + " " + animal.getName();
        storeEvent(event);

        petShopStorage.addAnimal(animal);
    }

    public void sell(Animal animal) {
        if (animal == null) {
            return;
        }

        balanceListStore();

        String event = "Selling of " + animal.getClass().getSimpleName() + " " + animal.getName();
        storeEvent(event);

        petShopStorage.deleteAnimal(animal);
    }

    private void storeEvent(String event) {
        String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
        String timeStampEvent = timeStamp + "   " + event;
        story.add(timeStampEvent);
        printingService.printEvent(timeStampEvent);
    }

    private void balanceListStore() {
        if (story.size() > Constants.MAX_COUNT_LIST_BUY_AND_SELL) {
            story.remove(0);
        }
    }

    public void buy() {
        buy(getRandomAnimal());
    }

    private Animal getRandomAnimal() {
        Animal animal = new Dog();

        switch (new Random().nextInt(4)) {
            case 0:
                animal = new Cat();
                break;
            case 1:
                animal = new Dog();
                break;
            case 2:
                animal = new Shark();
                break;
            case 3:
                animal = new Wolf();
                break;
        }

        return animal;
    }

    public void sell() {
        sell(petShopStorage.getRandomAnimalFromStorage());
    }

    public List<String> getStory() {
        return story;
    }

    public void setPetShopStorage(PetShopStorage petShopStorage) {
        this.petShopStorage = petShopStorage;
    }

    public PetShopStorage getPetShopStorage() {
        return petShopStorage;
    }

    public void setPrintingService(PrintingService printingService) {
        this.printingService = printingService;
    }

    public PrintingService getPrintingService() {
        return printingService;
    }
}
