package ru.petshop.services;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.petshop.Constants;
import ru.petshop.cipher.CaesarCipher;
import ru.petshop.models.Cat;
import ru.petshop.models.Dog;
import ru.petshop.models.Shark;
import ru.petshop.models.Wolf;
import ru.petshop.models.interfaces.Animal;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

@XmlRootElement
public class PetShopStorage {

    private List<Animal> animals;
    private List<String> runAwayAnimals;
    private PrintingService printingService;

    public PetShopStorage() {
        runAwayAnimals = new CopyOnWriteArrayList<>();
    }

    @XmlElements({
            @XmlElement(name = "cat", type = Cat.class),
            @XmlElement(name = "dog", type = Dog.class),
            @XmlElement(name = "wolf", type = Wolf.class),
            @XmlElement(name = "shark", type = Shark.class)
    })
    @XmlElementWrapper
    public List<Animal> getAnimals() {
        if (animals == null) {
            tryGetAnimalsList();
        }

        return animals;
    }

    private void tryGetAnimalsList() {
        animals = new CopyOnWriteArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        String initFile = getInitFilePath();
        try (BufferedReader br = new BufferedReader(new FileReader(initFile))) {

            String s;
            while ((s = br.readLine()) != null) {
                s = CaesarCipher.decrypt(s);
                animals = mapper.readValue(s, new TypeReference<CopyOnWriteArrayList<Animal>>() {
                });
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getInitFilePath() {
        ClassLoader classLoader = getClass().getClassLoader();
        return classLoader.getResource("init").getFile();
    }

    public List<String> getRunAwayAnimals() {
        return runAwayAnimals;
    }

    public void addAnimal(Animal animal) {
        animals.add(animal);
    }

    public void deleteAnimal(Animal animal) {
        animals.remove(animal);
    }

    public void runAway(Animal animal) {
        if (animal == null) {
            return;
        }

        deleteAnimal(animal);
    }

    public void runAway() {
        Animal animal = getRandomAnimalFromStorage();

        if (animal == null) {
            return;
        }

        balanceListRunAway();

        String event = animal.getClass().getSimpleName() + " " + animal.getName() + " run away!";
        storeEvent(event);

        runAway(animal);
    }

    public Animal getRandomAnimalFromStorage() {
        int size = animals.size();
        if (size == 0) {
            return null;
        }
        int pick = new Random().nextInt(size);
        return animals.get(pick);
    }

    private void storeEvent(String event) {
        String timeStamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Calendar.getInstance().getTime());
        String timeStampEvent = timeStamp + "   " + event;
        runAwayAnimals.add(timeStampEvent);
        printingService.printEvent(timeStampEvent);
    }

    private void balanceListRunAway() {
        if (runAwayAnimals.size() > Constants.MAX_COUNT_LIST_RUN_AWAY) {
            runAwayAnimals.remove(0);
        }
    }

    public void setPrintingService(PrintingService printingService) {
        this.printingService = printingService;
    }

    public PrintingService getPrintingService() {
        return printingService;
    }
}
