package ru.petshop.services;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class PrintingService {

    private String filePath;

    public PrintingService() {
        filePath = getPrintFilePath();
    }

    private String getPrintFilePath() {
        ClassLoader classLoader = getClass().getClassLoader();
        return classLoader.getResource("print").getFile();
    }

    public void printEvent(String event) {
        try {
            FileWriter writer = new FileWriter(filePath, true);
            BufferedWriter bufferWriter = new BufferedWriter(writer);
            bufferWriter.write(event + "\n");
            bufferWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
