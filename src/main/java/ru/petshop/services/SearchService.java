package ru.petshop.services;

import ru.petshop.models.interfaces.Animal;

import java.util.Optional;

public class SearchService {

    private PetShopStorage petShopStorage;

    public SearchService() {
    }

    public Optional<Animal> findAnimalByBreed(String breed) {
        return petShopStorage.getAnimals().stream()
                .filter(p -> p.getBreed().equals(breed)).findAny();
    }

    public Optional<Animal> findAnimalByName(String name) {
        return petShopStorage.getAnimals().stream()
                .filter(p -> p.getName().equals(name)).findAny();
    }

    public Optional<Animal> findAnimalByCost(Integer cost) {
        return petShopStorage.getAnimals().stream()
                .filter(p -> p.getCost().equals(cost)).findAny();
    }

    public Optional<Animal> findAnimalByCharacter(String character) {
        return petShopStorage.getAnimals().stream()
                .filter(p -> p.getCharacter().equals(character)).findAny();
    }

    public void setPetShopStorage(PetShopStorage petShopStorage) {
        this.petShopStorage = petShopStorage;
    }

    public PetShopStorage getPetShopStorage() {
        return petShopStorage;
    }
}
