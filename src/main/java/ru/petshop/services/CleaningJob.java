package ru.petshop.services;

import ru.petshop.Constants;
import ru.petshop.models.interfaces.Animal;

import java.util.List;
import java.util.Random;

public class CleaningJob implements Runnable {

    private PetShopStorage petShopStorage;

    public CleaningJob() {
    }

    public void run() {
        Random random = new Random();
        while (true) {
            sleep(500);

            cleaningAnimals(random);
        }
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void cleaningAnimals(Random random) {
        List<Animal> animals = petShopStorage.getAnimals();
        for (Animal animal : animals) {
            int degreeOfPollution = animal.getDegreeOfPollution();
            int pollution = randInt(random, Constants.MIN_STEP_CLEANING_OR_POLLUTION, Constants.MAX_STEP_CLEANING_OR_POLLUTION);
            degreeOfPollution -= pollution;
            animal.setDegreeOfPollution(degreeOfPollution < Constants.MIN_POLLUTION ? Constants.MIN_POLLUTION : degreeOfPollution);
        }
    }

    private int randInt(Random rand, int min, int max) {
        return rand.nextInt((max - min) + 1) + min;
    }

    public void setPetShopStorage(PetShopStorage petShopStorage) {
        this.petShopStorage = petShopStorage;
    }

    public PetShopStorage getPetShopStorage() {
        return petShopStorage;
    }
}
