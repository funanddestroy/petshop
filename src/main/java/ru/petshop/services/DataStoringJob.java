package ru.petshop.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.petshop.cipher.CaesarCipher;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.*;

public class DataStoringJob extends Thread {

    private PetShopStorage petShopStorage;

    public DataStoringJob() {
        start();
    }

    public void run() {
        while (true) {
            sleep(10000);

            storeAnimals();

            marshalXml();
        }
    }

    private void sleep(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void storeAnimals() {
        ClassLoader classLoader = getClass().getClassLoader();
        String initFile = classLoader.getResource("init").getFile();
        ObjectMapper mapper = new ObjectMapper();
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(initFile))) {
            String cipherAnimals = CaesarCipher.encrypt(mapper.writeValueAsString(petShopStorage.getAnimals()));
            bw.write(cipherAnimals);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void marshalXml() {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            String xmlFile = classLoader.getResource("animals.xml").getFile();
            JAXBContext jContext = JAXBContext.newInstance(PetShopStorage.class);
            Marshaller marshallObj = jContext.createMarshaller();
            marshallObj.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshallObj.marshal(petShopStorage, new File(xmlFile));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setPetShopStorage(PetShopStorage petShopStorage) {
        this.petShopStorage = petShopStorage;
    }

    public PetShopStorage getPetShopStorage() {
        return petShopStorage;
    }
}
