package ru.petshop;

/**
 * An interface that implements methods for working with animals.
 *
 * @version 0.1
 * @autor Poltavets Dmitry
 */
public interface Constants {

    /**
     * {@value #MAX_COAST} maximum price of an animal
     */
    Integer MAX_COAST = 50000;

    /**
     * {@value #MAX_COUNT_LIST_BUY_AND_SELL} maximum number of animals whose data are saved during the buying or selling
     */
    Integer MAX_COUNT_LIST_BUY_AND_SELL = 10;

    /**
     * {@value #MAX_COUNT_LIST_RUN_AWAY} maximum number of escaped animals the data of which are kept
     */
    Integer MAX_COUNT_LIST_RUN_AWAY = 5;

    /**
     * {@value #MAX_POLLUTION} maximum degree of pollution
     */
    Integer MAX_POLLUTION = 100;

    /**
     * {@value #MIN_POLLUTION} minimum  degree of pollution
     */
    Integer MIN_POLLUTION = 0;

    /**
     * {@value #MAX_STEP_CLEANING_OR_POLLUTION} maximum cleaning or pollution step
     */
    Integer MAX_STEP_CLEANING_OR_POLLUTION = 30;

    /**
     * {@value #MIN_STEP_CLEANING_OR_POLLUTION} minimum cleaning or pollution step
     */
    Integer MIN_STEP_CLEANING_OR_POLLUTION = 20;
}
