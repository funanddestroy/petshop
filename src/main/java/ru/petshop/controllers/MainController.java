package ru.petshop.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.petshop.services.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class MainController {

    @Autowired
    private PetShopStorage petShopStorage;
    @Autowired
    private BuyAndSell buyAndSell;
    @Autowired
    private RealLifeEmulator realLifeEmulator;
    @Autowired
    private DataStoringJob dataStoringJob;
    @Autowired
    private PollutionJob pollutionJob;
    @Autowired
    private CleaningJob cleaningJob;

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public ModelAndView main(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("animals", petShopStorage.getAnimals());
        modelAndView.addObject("run_away_animals", petShopStorage.getRunAwayAnimals());
        modelAndView.addObject("story", buyAndSell.getStory());

        new Thread(dataStoringJob).start();
        new Thread(realLifeEmulator).start();
        new Thread(pollutionJob).start();
        new Thread(cleaningJob).start();

        modelAndView.setViewName("index");
        return modelAndView;
    }

    @RequestMapping(value = "/index", method = RequestMethod.POST)
    public ModelAndView buyOrSell(HttpServletRequest request, HttpServletResponse response) {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("index");

        String status = request.getParameter("status");

        switch (status) {
            case "buy":
                buyAndSell.buy();
                break;
            case "sell":
                buyAndSell.sell();
                break;
        }

        modelAndView.addObject("animals", petShopStorage.getAnimals());
        modelAndView.addObject("run_away_animals", petShopStorage.getRunAwayAnimals());
        modelAndView.addObject("story", buyAndSell.getStory());

        return modelAndView;
    }

}
